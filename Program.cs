﻿using System;

namespace M3UF4Proba_MartinezArmando
{
    public interface IPokemon
    {
        void Atacar(IPokemon obj);
        void Curar();
    }
    public abstract class Pokemon : IPokemon
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Nivel { get; set; }
        public int VidaMaxima { get; set; }
        public int VidaActual { get; set; }

        public Pokemon(string nombre, string tipo, int nivel, int vida_max, int vida)
        {
            this.Nombre = nombre;
            this.Tipo = tipo;
            this.Nivel = nivel;
            this.VidaMaxima = vida_max;
            this.VidaActual = vida;
        }
        public void Atacar(IPokemon obj)
        {
            Pokemon enemy = (Pokemon)obj;
            Console.WriteLine($"{this.Nombre} ataca a {enemy.Nombre}\nInflinge {this.AtaquePoder()} puntos de daño!");
            enemy.VidaActual -= this.AtaquePoder();
        }
        public void Curar()
        {
            int n = 3;
            if (this.VidaActual + 3 <= this.VidaMaxima) { this.VidaActual += n; }
            else
            {
                n = this.VidaMaxima - this.VidaActual;
                this.VidaActual = this.VidaMaxima;
            }
            Console.WriteLine($"{this.Nombre} se cura.\nRecupera {n} puntos de vida!");
        }
        public abstract int AtaquePoder();
    }
    public class PokemonAgua : Pokemon
    {
        public int PoderAgua { get; set; }
        public PokemonAgua(string nombre, string tipo, int nivel, int vida_max, int vida, int poderAgua) : base (nombre, tipo, nivel, vida_max, vida)
        {
            this.PoderAgua = poderAgua;
        }
        override public int AtaquePoder()
        {
            return this.Nivel * this.PoderAgua;
        }
    }
    public class PokemonFuego : Pokemon
    {
        public int PoderFuego { get; set; }
        public PokemonFuego(string nombre, string tipo, int nivel, int vida_max, int vida, int poderFuego) : base(nombre, tipo, nivel, vida_max, vida)
        {
            this.PoderFuego = poderFuego;
        }
        override public int AtaquePoder()
        {
            return this.Nivel * this.PoderFuego;
        }
    }
    public class PokemonPlanta : Pokemon
    {
        public int PoderPlanta { get; set; }
        public PokemonPlanta(string nombre, string tipo, int nivel, int vida_max, int vida, int poderPlanta) : base(nombre, tipo, nivel, vida_max, vida)
        {
            this.PoderPlanta = poderPlanta;
        }
        override public int AtaquePoder()
        {
            return this.Nivel * this.PoderPlanta;
        }
    }
    class PruebasEstadioPokemon
    {
        static void Main()
        {
            Pokemon[] pokedex =
            {
                new PokemonAgua("Squirtle", "agua", 5, 18, 18, 2),
                new PokemonFuego("Charmander", "fuego", 5, 24, 24, 2),
                new PokemonPlanta("Bulbasaur", "planta", 5, 27, 27, 3),
                new PokemonFuego("Rapidash", "fuego", 27, 130, 109, 4),
                new PokemonAgua("Gyarados", "agua", 31, 98, 86, 4),
                new PokemonPlanta("Vileplum", "planta", 26, 112, 97, 3)
            };

            Pokemon player = pokedex[0];
            Pokemon enemy = pokedex[0];
            string[] options = new string[] { "Selecciona a tu Pokémon", "Selecciona el pokémon al que te enfrentarás" };

            for (int j = 0; j < options.Length; j++)
            {
                bool selected;
                do
                {
                    selected = true;
                    Console.WriteLine(options[j]);
                    for (int i = 0; i < pokedex.Length; i++)
                    {
                        Console.WriteLine($"{i + 1}. {pokedex[i].Nombre}(lvl {pokedex[i].Nivel})");
                    }
                    switch (Console.ReadLine())
                    {
                        case "1":
                            if (j == 0) player = pokedex[0];
                            else enemy = pokedex[0];
                            break;
                        case "2":
                            if (j == 0) player = pokedex[1];
                            else enemy = pokedex[1];
                            break;
                        case "3":
                            if (j == 0) player = pokedex[2];
                            else enemy = pokedex[2];
                            break;
                        case "4":
                            if (j == 0) player = pokedex[3];
                            else enemy = pokedex[3];
                            break;
                        case "5":
                            if (j == 0) player = pokedex[4];
                            else enemy = pokedex[4];
                            break;
                        case "6":
                            if (j == 0) player = pokedex[5];
                            else enemy = pokedex[5];
                            break;
                        default:
                            Console.WriteLine("ERROR: SELECCIÓN INCORRECTA");
                            selected = false;
                            break;
                    }
                    if (selected && j == 0) Console.WriteLine($"Has seleccionado a {player.Nombre}, del tipo {player.Tipo}");
                    else if (selected && j == 1) Console.WriteLine($"Has seleccionado a {enemy.Nombre}, del tipo {enemy.Tipo}");
                } while (!selected);
                Console.WriteLine("============================");
            }
            Console.ReadLine();
            Console.Clear();
            var rand = new Random();

            Console.WriteLine($"\n{player.Nombre}(lvl {player.Nivel}):{player.VidaActual}/{player.VidaMaxima}\n{enemy.Nombre}(lvl {enemy.Nivel}):{enemy.VidaActual}/{enemy.VidaMaxima}");
            do
            {
                Console.WriteLine("Elige que hacer:");
                foreach (string option in new string[] { "1. Atacar", "2. Ataque Elemental", "3. Curar" })
                {
                    Console.WriteLine(option);
                }
                bool selected;
                do
                {
                    selected = true;
                    switch (Console.ReadLine()){
                        case "1":
                            Console.WriteLine($"{player.Nombre} ataca a {enemy.Nombre} e inflinge {player.Nivel} puntos de daño.");
                            enemy.VidaActual -= player.Nivel;
                            break;
                        case "2":
                            AtaqueElemental(player, enemy);
                            break;
                        case "3":
                            player.Curar();
                            break;
                        default:
                            selected = false;
                            break;
                    }
                } while (!selected);

                Console.WriteLine($"\n{player.Nombre}(lvl {player.Nivel}):{player.VidaActual}/{player.VidaMaxima}\n{enemy.Nombre}(lvl {enemy.Nivel}):{enemy.VidaActual}/{enemy.VidaMaxima}");
                if (enemy.VidaActual < 0) break;

                switch(rand.Next(1, 4))
                {
                    case 1:
                        Console.WriteLine($"{enemy.Nombre} ataca a {player.Nombre} e inflinge {enemy.Nivel} puntos de daño.");
                        player.VidaActual -= enemy.Nivel;
                        break;
                    case 2:
                        AtaqueElemental(enemy, player);
                        break;
                    case 3:
                        enemy.Curar();
                        break;
                }

                Console.WriteLine($"\n{player.Nombre}(lvl {player.Nivel}):{player.VidaActual}/{player.VidaMaxima}\n{enemy.Nombre}(lvl {enemy.Nivel}):{enemy.VidaActual}/{enemy.VidaMaxima}");
            } while (player.VidaActual > 0 && enemy.VidaActual > 0);
            if (player.VidaActual > 0) Console.WriteLine("¡Felicidades! ¡Has ganado!");
            else Console.WriteLine("¡Has perdido! ¡Prueba a revisar la tabla de tipos!");
            Console.ReadLine();

        }

        static void AtaqueElemental(IPokemon atacante, IPokemon defensor)
        {
            Pokemon pokeAtacante = (Pokemon)atacante;
            Pokemon pokeDefensor = (Pokemon)defensor;
            bool effective = false;

            switch (pokeAtacante.Tipo)
            {
                case "agua":
                    if (pokeDefensor.Tipo == "fuego") { effective = true; }
                    break;
                case "fuego":
                    if (pokeDefensor.Tipo == "planta") { effective = true; }
                    break;
                case "planta":
                    if (pokeDefensor.Tipo == "agua") { effective = true; }
                    break;
            }
            
            if (effective)
            {
                Console.WriteLine($"¡{pokeAtacante.Nombre} ataca con poder de {pokeAtacante.Tipo}!");
                pokeAtacante.Atacar(pokeDefensor);
            } 
            else
            {
                Console.WriteLine($"No hay ventaja elemental, el daño causado es el nivel del atacante: {pokeAtacante.Nivel}");
                pokeDefensor.VidaActual -= pokeAtacante.Nivel;
            }
        }
    }
}
